#[allow(dead_code)]
mod ffi {
    extern "C" {
        pub fn floatingrandom_typeaf(sign: i8, r: u32) -> f32;
        pub fn floatingrandom_typebf(sign: i8, r: u32) -> f32;
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn typeb() {
        let mut vec: Vec<f32> = vec![];

        for _ in 0..1000 {
            vec.push(unsafe { crate::ffi::floatingrandom_typebf(false as _, rand::random()) });
        }

        vec.sort_by(|a, b| a.partial_cmp(b).unwrap());

        let mut sum: f32 = 0.0;

        for f in vec.iter() {
            sum += f;
        }

        println!("{}", sum);

        assert!(45.0 > sum && sum > 10.0);
    }
}
