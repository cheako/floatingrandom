use std::path::Path;

fn get_time(p: &Path) -> std::time::SystemTime {
    p.metadata()
        .expect("failed to get metadata")
        .modified()
        .expect("failed to get modified")
}

fn main() {
    let configure = Path::new("./configure");
    assert!(configure.is_file());

    let makefile = Path::new("./Makefile");
    if !(makefile.is_file() && get_time(&makefile) > get_time(&configure)) {
        let mut command = std::process::Command::new("./configure");

        let mut child = command.spawn().expect("failed to execute configure");
        let ecode = child.wait().expect("failed to wait on configure");

        assert!(ecode.success());
    }

    let mut command = make_cmd::make();
    let mut child = command.spawn().expect("failed to execute make");
    let ecode = child.wait().expect("failed to wait on make");

    assert!(ecode.success());

    println!("cargo:rustc-link-search=native=src");
    println!("cargo:rustc-link-lib=static=floatingrandom");
}
