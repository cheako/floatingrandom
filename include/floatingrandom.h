/*
MIT License

Copyright (c) 2019 Michael Mestnik

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#ifndef __FLOATINGRANDOM_H__
#define __FLOATINGRANDOM_H__

// clang-format off
#ifdef __cplusplus
extern "C" {
#endif
        // clang-format on

#include <stdint.h>
#include <stdbool.h>

        // floatingrandom_intint2u32(rand(), rand()), a is most significant.
        inline uint32_t floatingrandom_intint2u32(int a, int b)
        {
                return (b & 0xffff) + ((a & 0xffff) << 16);
        }

        // Takes 24bits and returns value from 0 to 1 or -1 if bool is true.
        float floatingrandom_typeaf(char, uint32_t);
        // Takes 30bits and returns value from 0 to 1 or -1 if bool is true.
        float floatingrandom_typebf(char, uint32_t);

#ifdef __cplusplus
}
#endif

#endif /*__FLOATINGRANDOM_H__*/